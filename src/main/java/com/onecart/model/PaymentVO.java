package com.onecart.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Alvin on 2015/4/4.
 */
@Entity
@Table(name = "payment", schema = "", catalog = "onecart")
public class PaymentVO implements Serializable {
    private int paymentId;
    private String orderId;
    private String email;
    private String payaccount;
    private int amount;
    private Timestamp addDate;
    private Timestamp dueDate;
    private String settled;

    @Id
    @Column(name = "payment_id")
    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    @Basic
    @Column(name = "order_id")
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "payaccount")
    public String getPayaccount() {
        return payaccount;
    }

    public void setPayaccount(String payaccount) {
        this.payaccount = payaccount;
    }

    @Basic
    @Column(name = "amount")
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "add_date")
    public Timestamp getAddDate() {
        return addDate;
    }

    public void setAddDate(Timestamp addDate) {
        this.addDate = addDate;
    }

    @Basic
    @Column(name = "due_date")
    public Timestamp getDueDate() {
        return dueDate;
    }

    public void setDueDate(Timestamp dueDate) {
        this.dueDate = dueDate;
    }

    @Basic
    @Column(name = "settled")
    public String getSettled() {
        return settled;
    }

    public void setSettled(String settled) {
        this.settled = settled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentVO paymentVO = (PaymentVO) o;

        if (paymentId != paymentVO.paymentId) return false;
        if (amount != paymentVO.amount) return false;
        if (orderId != null ? !orderId.equals(paymentVO.orderId) : paymentVO.orderId != null) return false;
        if (email != null ? !email.equals(paymentVO.email) : paymentVO.email != null) return false;
        if (payaccount != null ? !payaccount.equals(paymentVO.payaccount) : paymentVO.payaccount != null) return false;
        if (addDate != null ? !addDate.equals(paymentVO.addDate) : paymentVO.addDate != null) return false;
        if (dueDate != null ? !dueDate.equals(paymentVO.dueDate) : paymentVO.dueDate != null) return false;
        if (settled != null ? !settled.equals(paymentVO.settled) : paymentVO.settled != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = paymentId;
        result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (payaccount != null ? payaccount.hashCode() : 0);
        result = 31 * result + amount;
        result = 31 * result + (addDate != null ? addDate.hashCode() : 0);
        result = 31 * result + (dueDate != null ? dueDate.hashCode() : 0);
        result = 31 * result + (settled != null ? settled.hashCode() : 0);
        return result;
    }
}
