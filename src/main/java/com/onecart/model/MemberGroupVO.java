package com.onecart.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Alvin on 2015/4/4.
 */
@Entity
@Table(name = "member_group", schema = "", catalog = "onecart")
public class MemberGroupVO implements Serializable {
    private int groupId;
    private String groupName;
    private int status;
    private String permission;

    @Id
    @Column(name = "group_id")
    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    @Basic
    @Column(name = "group_name")
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Basic
    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @Column(name = "permission")
    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MemberGroupVO that = (MemberGroupVO) o;

        if (groupId != that.groupId) return false;
        if (status != that.status) return false;
        if (groupName != null ? !groupName.equals(that.groupName) : that.groupName != null) return false;
        if (permission != null ? !permission.equals(that.permission) : that.permission != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = groupId;
        result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
        result = 31 * result + status;
        result = 31 * result + (permission != null ? permission.hashCode() : 0);
        return result;
    }
}
