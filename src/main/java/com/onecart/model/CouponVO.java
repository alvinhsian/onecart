package com.onecart.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Alvin on 2015/4/4.
 */
@Entity
@Table(name = "coupon", schema = "", catalog = "onecart")
public class CouponVO implements Serializable {
    private String couponCode;
    private Integer discount;
    private Integer price;
    private Date addDate;
    private Date usedDate;
    private Date expDate;
    private int status;

    @Id
    @Column(name = "coupon_code")
    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    @Basic
    @Column(name = "discount")
    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    @Basic
    @Column(name = "price")
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Basic
    @Column(name = "add_date")
    public Date getAddDate() {
        return addDate;
    }

    public void setAddDate(Date addDate) {
        this.addDate = addDate;
    }

    @Basic
    @Column(name = "used_date")
    public Date getUsedDate() {
        return usedDate;
    }

    public void setUsedDate(Date usedDate) {
        this.usedDate = usedDate;
    }

    @Basic
    @Column(name = "exp_date")
    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    @Basic
    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CouponVO couponVO = (CouponVO) o;

        if (status != couponVO.status) return false;
        if (couponCode != null ? !couponCode.equals(couponVO.couponCode) : couponVO.couponCode != null) return false;
        if (discount != null ? !discount.equals(couponVO.discount) : couponVO.discount != null) return false;
        if (price != null ? !price.equals(couponVO.price) : couponVO.price != null) return false;
        if (addDate != null ? !addDate.equals(couponVO.addDate) : couponVO.addDate != null) return false;
        if (usedDate != null ? !usedDate.equals(couponVO.usedDate) : couponVO.usedDate != null) return false;
        if (expDate != null ? !expDate.equals(couponVO.expDate) : couponVO.expDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = couponCode != null ? couponCode.hashCode() : 0;
        result = 31 * result + (discount != null ? discount.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (addDate != null ? addDate.hashCode() : 0);
        result = 31 * result + (usedDate != null ? usedDate.hashCode() : 0);
        result = 31 * result + (expDate != null ? expDate.hashCode() : 0);
        result = 31 * result + status;
        return result;
    }
}
