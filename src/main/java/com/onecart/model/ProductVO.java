package com.onecart.model;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by Alvin on 2015/4/4.
 */
@Entity
@javax.persistence.Table(name = "product", schema = "", catalog = "onecart")
public class ProductVO implements Serializable {
    private int productId;

    @Id
    @javax.persistence.Column(name = "product_id")
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    private String name;

    @Basic
    @javax.persistence.Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String model;

    @Basic
    @javax.persistence.Column(name = "model")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    private int quantity;

    @Basic
    @javax.persistence.Column(name = "quantity")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    private Integer discount;

    @Basic
    @javax.persistence.Column(name = "discount")
    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    private int price;

    @Basic
    @javax.persistence.Column(name = "price")
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    private int isbn;

    @Basic
    @javax.persistence.Column(name = "isbn")
    public int getIsbn() {
        return isbn;
    }

    public void setIsbn(int isbn) {
        this.isbn = isbn;
    }

    private int manufacturerId;

    @Basic
    @javax.persistence.Column(name = "manufacturer_id")
    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    private Date dateAvailable;

    @Basic
    @javax.persistence.Column(name = "date_available")
    public Date getDateAvailable() {
        return dateAvailable;
    }

    public void setDateAvailable(Date dateAvailable) {
        this.dateAvailable = dateAvailable;
    }

    private int weight;

    @Basic
    @javax.persistence.Column(name = "weight")
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    private int length;

    @Basic
    @javax.persistence.Column(name = "length")
    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    private int width;

    @Basic
    @javax.persistence.Column(name = "width")
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    private int height;

    @Basic
    @javax.persistence.Column(name = "height")
    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    private byte status;

    @Basic
    @javax.persistence.Column(name = "status")
    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    private Timestamp dateAdded;

    @Basic
    @javax.persistence.Column(name = "date_added")
    public Timestamp getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Timestamp dateAdded) {
        this.dateAdded = dateAdded;
    }

    private Timestamp dateModified;

    @Basic
    @javax.persistence.Column(name = "date_modified")
    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

    private int point;

    @Basic
    @javax.persistence.Column(name = "point")
    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    private int minimum;

    @Basic
    @javax.persistence.Column(name = "minimum")
    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    private int categoryId;

    @Basic
    @javax.persistence.Column(name = "category_id")
    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    private String image1;

    @Basic
    @javax.persistence.Column(name = "image1")
    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    private String image2;

    @Basic
    @javax.persistence.Column(name = "image2")
    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    private String image3;

    @Basic
    @javax.persistence.Column(name = "image3")
    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    private String image4;

    @Basic
    @javax.persistence.Column(name = "image4")
    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    private String image5;

    @Basic
    @javax.persistence.Column(name = "image5")
    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    private String image6;

    @Basic
    @javax.persistence.Column(name = "image6")
    public String getImage6() {
        return image6;
    }

    public void setImage6(String image6) {
        this.image6 = image6;
    }

    private String description;

    @Basic
    @javax.persistence.Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductVO productVO = (ProductVO) o;

        if (productId != productVO.productId) return false;
        if (quantity != productVO.quantity) return false;
        if (price != productVO.price) return false;
        if (isbn != productVO.isbn) return false;
        if (manufacturerId != productVO.manufacturerId) return false;
        if (weight != productVO.weight) return false;
        if (length != productVO.length) return false;
        if (width != productVO.width) return false;
        if (height != productVO.height) return false;
        if (status != productVO.status) return false;
        if (point != productVO.point) return false;
        if (minimum != productVO.minimum) return false;
        if (categoryId != productVO.categoryId) return false;
        if (name != null ? !name.equals(productVO.name) : productVO.name != null) return false;
        if (model != null ? !model.equals(productVO.model) : productVO.model != null) return false;
        if (discount != null ? !discount.equals(productVO.discount) : productVO.discount != null) return false;
        if (dateAvailable != null ? !dateAvailable.equals(productVO.dateAvailable) : productVO.dateAvailable != null)
            return false;
        if (dateAdded != null ? !dateAdded.equals(productVO.dateAdded) : productVO.dateAdded != null) return false;
        if (dateModified != null ? !dateModified.equals(productVO.dateModified) : productVO.dateModified != null)
            return false;
        if (image1 != null ? !image1.equals(productVO.image1) : productVO.image1 != null) return false;
        if (image2 != null ? !image2.equals(productVO.image2) : productVO.image2 != null) return false;
        if (image3 != null ? !image3.equals(productVO.image3) : productVO.image3 != null) return false;
        if (image4 != null ? !image4.equals(productVO.image4) : productVO.image4 != null) return false;
        if (image5 != null ? !image5.equals(productVO.image5) : productVO.image5 != null) return false;
        if (image6 != null ? !image6.equals(productVO.image6) : productVO.image6 != null) return false;
        if (description != null ? !description.equals(productVO.description) : productVO.description != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = productId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + quantity;
        result = 31 * result + (discount != null ? discount.hashCode() : 0);
        result = 31 * result + price;
        result = 31 * result + isbn;
        result = 31 * result + manufacturerId;
        result = 31 * result + (dateAvailable != null ? dateAvailable.hashCode() : 0);
        result = 31 * result + weight;
        result = 31 * result + length;
        result = 31 * result + width;
        result = 31 * result + height;
        result = 31 * result + (int) status;
        result = 31 * result + (dateAdded != null ? dateAdded.hashCode() : 0);
        result = 31 * result + (dateModified != null ? dateModified.hashCode() : 0);
        result = 31 * result + point;
        result = 31 * result + minimum;
        result = 31 * result + categoryId;
        result = 31 * result + (image1 != null ? image1.hashCode() : 0);
        result = 31 * result + (image2 != null ? image2.hashCode() : 0);
        result = 31 * result + (image3 != null ? image3.hashCode() : 0);
        result = 31 * result + (image4 != null ? image4.hashCode() : 0);
        result = 31 * result + (image5 != null ? image5.hashCode() : 0);
        result = 31 * result + (image6 != null ? image6.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
