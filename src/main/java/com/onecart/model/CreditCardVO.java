package com.onecart.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Alvin on 2015/4/4.
 */
@Entity
@Table(name = "credit_card", schema = "", catalog = "onecart")
public class CreditCardVO implements Serializable {
    private int cid;
    private String email;
    private int cardNumber;
    private int type;
    private Timestamp expireDate;
    private String memberEmail;

    @Id
    @Column(name = "cid")
    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "card_number")
    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "expire_date")
    public Timestamp getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Timestamp expireDate) {
        this.expireDate = expireDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreditCardVO that = (CreditCardVO) o;

        if (cid != that.cid) return false;
        if (cardNumber != that.cardNumber) return false;
        if (type != that.type) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (expireDate != null ? !expireDate.equals(that.expireDate) : that.expireDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cid;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + cardNumber;
        result = 31 * result + type;
        result = 31 * result + (expireDate != null ? expireDate.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "member_email")
    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }
}
