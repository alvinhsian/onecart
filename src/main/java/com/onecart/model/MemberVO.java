package com.onecart.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Alvin on 2015/4/4.
 */
@Entity
@Table(name = "member", schema = "", catalog = "onecart")
public class MemberVO implements Serializable {
    private Timestamp memberId;
    private String email;
    private String firstname;
    private String lastname;
    private String password;
    private int groupId;
    private int status;
    private Timestamp addDate;
    private Timestamp modifyDate;
    private String gender;
    private String address;
    private String phonenumber;
    private int point;
    private Timestamp loginTime;
    private String loginIp;
    private int memberGroupGroupId;

    @Basic
    @Column(name = "member_id")
    public Timestamp getMemberId() {
        return memberId;
    }

    public void setMemberId(Timestamp memberId) {
        this.memberId = memberId;
    }

    @Id
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "firstname")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Basic
    @Column(name = "lastname")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "group_id")
    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    @Basic
    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @Column(name = "add_date")
    public Timestamp getAddDate() {
        return addDate;
    }

    public void setAddDate(Timestamp addDate) {
        this.addDate = addDate;
    }

    @Basic
    @Column(name = "modify_date")
    public Timestamp getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Timestamp modifyDate) {
        this.modifyDate = modifyDate;
    }

    @Basic
    @Column(name = "gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "phonenumber")
    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    @Basic
    @Column(name = "point")
    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Basic
    @Column(name = "login_time")
    public Timestamp getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Timestamp loginTime) {
        this.loginTime = loginTime;
    }

    @Basic
    @Column(name = "login_ip")
    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MemberVO memberVO = (MemberVO) o;

        if (groupId != memberVO.groupId) return false;
        if (status != memberVO.status) return false;
        if (point != memberVO.point) return false;
        if (memberId != null ? !memberId.equals(memberVO.memberId) : memberVO.memberId != null) return false;
        if (email != null ? !email.equals(memberVO.email) : memberVO.email != null) return false;
        if (firstname != null ? !firstname.equals(memberVO.firstname) : memberVO.firstname != null) return false;
        if (lastname != null ? !lastname.equals(memberVO.lastname) : memberVO.lastname != null) return false;
        if (password != null ? !password.equals(memberVO.password) : memberVO.password != null) return false;
        if (addDate != null ? !addDate.equals(memberVO.addDate) : memberVO.addDate != null) return false;
        if (modifyDate != null ? !modifyDate.equals(memberVO.modifyDate) : memberVO.modifyDate != null) return false;
        if (gender != null ? !gender.equals(memberVO.gender) : memberVO.gender != null) return false;
        if (address != null ? !address.equals(memberVO.address) : memberVO.address != null) return false;
        if (phonenumber != null ? !phonenumber.equals(memberVO.phonenumber) : memberVO.phonenumber != null)
            return false;
        if (loginTime != null ? !loginTime.equals(memberVO.loginTime) : memberVO.loginTime != null) return false;
        if (loginIp != null ? !loginIp.equals(memberVO.loginIp) : memberVO.loginIp != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = memberId != null ? memberId.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + groupId;
        result = 31 * result + status;
        result = 31 * result + (addDate != null ? addDate.hashCode() : 0);
        result = 31 * result + (modifyDate != null ? modifyDate.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (phonenumber != null ? phonenumber.hashCode() : 0);
        result = 31 * result + point;
        result = 31 * result + (loginTime != null ? loginTime.hashCode() : 0);
        result = 31 * result + (loginIp != null ? loginIp.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "member_group_group_id")
    public int getMemberGroupGroupId() {
        return memberGroupGroupId;
    }

    public void setMemberGroupGroupId(int memberGroupGroupId) {
        this.memberGroupGroupId = memberGroupGroupId;
    }
}
