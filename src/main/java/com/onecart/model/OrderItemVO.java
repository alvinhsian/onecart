package com.onecart.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Alvin on 2015/4/4.
 */
@Entity
@Table(name = "order_item", schema = "", catalog = "onecart")
@IdClass(OrderItemVOPK.class)
public class OrderItemVO implements Serializable {
    private String orderId;
    private int productId;
    private String name;
    private String model;
    private int quantity;
    private int price;
    private int total;
    private String orderOrderId;

    @Id
    @Column(name = "order_id")
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Id
    @Column(name = "product_id")
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "model")
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Basic
    @Column(name = "quantity")
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "price")
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Basic
    @Column(name = "total")
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderItemVO that = (OrderItemVO) o;

        if (productId != that.productId) return false;
        if (quantity != that.quantity) return false;
        if (price != that.price) return false;
        if (total != that.total) return false;
        if (orderId != null ? !orderId.equals(that.orderId) : that.orderId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (model != null ? !model.equals(that.model) : that.model != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderId != null ? orderId.hashCode() : 0;
        result = 31 * result + productId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + quantity;
        result = 31 * result + price;
        result = 31 * result + total;
        return result;
    }

    @Basic
    @Column(name = "order_order_id")
    public String getOrderOrderId() {
        return orderOrderId;
    }

    public void setOrderOrderId(String orderOrderId) {
        this.orderOrderId = orderOrderId;
    }
}
