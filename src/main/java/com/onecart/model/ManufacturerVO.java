package com.onecart.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Alvin on 2015/4/4.
 */
@Entity
@Table(name = "manufacturer", schema = "", catalog = "onecart")
public class ManufacturerVO implements Serializable {
    private int manufacturerId;
    private String manufacturerName;

    @Id
    @Column(name = "manufacturer_id")
    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    @Basic
    @Column(name = "manufacturer_name")
    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ManufacturerVO that = (ManufacturerVO) o;

        if (manufacturerId != that.manufacturerId) return false;
        if (manufacturerName != null ? !manufacturerName.equals(that.manufacturerName) : that.manufacturerName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = manufacturerId;
        result = 31 * result + (manufacturerName != null ? manufacturerName.hashCode() : 0);
        return result;
    }
}
