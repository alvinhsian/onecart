package com.onecart.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Alvin on 2015/4/4.
 */
@Entity
@Table(name = "order", schema = "", catalog = "onecart")
public class OrderVO implements Serializable {
    private String orderId;
    private String email;
    private String shippingFirstname;
    private String shippingLastname;
    private String phonenumber;
    private String national;
    private String city;
    private int zipcode;
    private String shippingAddress;
    private String trackingCode;
    private String comment;
    private Timestamp addTime;
    private Timestamp paidTime;
    private Timestamp modifyTime;
    private String ip;
    private String couponCode;
    private int paymentPaymentId;
    private String memberEmail;
    private String couponCouponCode;

    @Id
    @Column(name = "order_id")
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "shipping_firstname")
    public String getShippingFirstname() {
        return shippingFirstname;
    }

    public void setShippingFirstname(String shippingFirstname) {
        this.shippingFirstname = shippingFirstname;
    }

    @Basic
    @Column(name = "shipping_lastname")
    public String getShippingLastname() {
        return shippingLastname;
    }

    public void setShippingLastname(String shippingLastname) {
        this.shippingLastname = shippingLastname;
    }

    @Basic
    @Column(name = "phonenumber")
    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    @Basic
    @Column(name = "national")
    public String getNational() {
        return national;
    }

    public void setNational(String national) {
        this.national = national;
    }

    @Basic
    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "zipcode")
    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    @Basic
    @Column(name = "shipping_address")
    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    @Basic
    @Column(name = "tracking_code")
    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Basic
    @Column(name = "add_time")
    public Timestamp getAddTime() {
        return addTime;
    }

    public void setAddTime(Timestamp addTime) {
        this.addTime = addTime;
    }

    @Basic
    @Column(name = "paid_time")
    public Timestamp getPaidTime() {
        return paidTime;
    }

    public void setPaidTime(Timestamp paidTime) {
        this.paidTime = paidTime;
    }

    @Basic
    @Column(name = "modify_time")
    public Timestamp getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Timestamp modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Basic
    @Column(name = "ip")
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Basic
    @Column(name = "coupon_code")
    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderVO orderVO = (OrderVO) o;

        if (zipcode != orderVO.zipcode) return false;
        if (orderId != null ? !orderId.equals(orderVO.orderId) : orderVO.orderId != null) return false;
        if (email != null ? !email.equals(orderVO.email) : orderVO.email != null) return false;
        if (shippingFirstname != null ? !shippingFirstname.equals(orderVO.shippingFirstname) : orderVO.shippingFirstname != null)
            return false;
        if (shippingLastname != null ? !shippingLastname.equals(orderVO.shippingLastname) : orderVO.shippingLastname != null)
            return false;
        if (phonenumber != null ? !phonenumber.equals(orderVO.phonenumber) : orderVO.phonenumber != null) return false;
        if (national != null ? !national.equals(orderVO.national) : orderVO.national != null) return false;
        if (city != null ? !city.equals(orderVO.city) : orderVO.city != null) return false;
        if (shippingAddress != null ? !shippingAddress.equals(orderVO.shippingAddress) : orderVO.shippingAddress != null)
            return false;
        if (trackingCode != null ? !trackingCode.equals(orderVO.trackingCode) : orderVO.trackingCode != null)
            return false;
        if (comment != null ? !comment.equals(orderVO.comment) : orderVO.comment != null) return false;
        if (addTime != null ? !addTime.equals(orderVO.addTime) : orderVO.addTime != null) return false;
        if (paidTime != null ? !paidTime.equals(orderVO.paidTime) : orderVO.paidTime != null) return false;
        if (modifyTime != null ? !modifyTime.equals(orderVO.modifyTime) : orderVO.modifyTime != null) return false;
        if (ip != null ? !ip.equals(orderVO.ip) : orderVO.ip != null) return false;
        if (couponCode != null ? !couponCode.equals(orderVO.couponCode) : orderVO.couponCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderId != null ? orderId.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (shippingFirstname != null ? shippingFirstname.hashCode() : 0);
        result = 31 * result + (shippingLastname != null ? shippingLastname.hashCode() : 0);
        result = 31 * result + (phonenumber != null ? phonenumber.hashCode() : 0);
        result = 31 * result + (national != null ? national.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + zipcode;
        result = 31 * result + (shippingAddress != null ? shippingAddress.hashCode() : 0);
        result = 31 * result + (trackingCode != null ? trackingCode.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (addTime != null ? addTime.hashCode() : 0);
        result = 31 * result + (paidTime != null ? paidTime.hashCode() : 0);
        result = 31 * result + (modifyTime != null ? modifyTime.hashCode() : 0);
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (couponCode != null ? couponCode.hashCode() : 0);
        return result;
    }

    @Basic
    @Column(name = "payment_payment_id")
    public int getPaymentPaymentId() {
        return paymentPaymentId;
    }

    public void setPaymentPaymentId(int paymentPaymentId) {
        this.paymentPaymentId = paymentPaymentId;
    }

    @Basic
    @Column(name = "member_email")
    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    @Basic
    @Column(name = "coupon_coupon_code")
    public String getCouponCouponCode() {
        return couponCouponCode;
    }

    public void setCouponCouponCode(String couponCouponCode) {
        this.couponCouponCode = couponCouponCode;
    }
}
