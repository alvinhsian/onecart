package com.onecart.dao;

import com.generic.dao.GenericDAO;
import com.onecart.model.CategoryVO;

/**
 * Created by Alvin on 2015/4/4.
 */
public interface CategoryDAO extends GenericDAO<CategoryVO, Integer> {
}
