package com.onecart.dao;

import com.generic.dao.GenericDAO;
import com.onecart.model.MemberGroupVO;

/**
 * Created by Alvin on 2015/4/4.
 */
public interface MemberGroupDAO extends GenericDAO<MemberGroupVO, Integer> {
}
