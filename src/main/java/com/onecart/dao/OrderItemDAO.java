package com.onecart.dao;

import com.generic.dao.GenericDAO;
import com.onecart.model.OrderItemVO;

/**
 * Created by Alvin on 2015/4/4.
 */
public interface OrderItemDAO extends GenericDAO<OrderItemVO, String> {
}
