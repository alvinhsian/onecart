package com.onecart.dao;

import com.generic.dao.GenericDAO;
import com.onecart.model.CouponVO;

/**
 * Created by Alvin on 2015/4/4.
 */
public interface CouponDAO extends GenericDAO<CouponVO, String> {
}
