package com.onecart.dao;

import com.generic.dao.GenericDAO;
import com.onecart.model.MemberVO;

/**
 * Created by Alvin on 2015/4/4.
 */
public interface MemberDAO extends GenericDAO<MemberVO, String> {
}
