package com.onecart.dao;

import com.generic.dao.GenericDAO;
import com.onecart.model.PaymentVO;

/**
 * Created by Alvin on 2015/4/4.
 */
public interface PaymentDAO extends GenericDAO<PaymentVO, Integer> {
}
