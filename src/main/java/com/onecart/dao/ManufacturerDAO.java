package com.onecart.dao;

import com.generic.dao.GenericDAO;
import com.onecart.model.ManufacturerVO;

/**
 * Created by Alvin on 2015/4/4.
 */
public interface ManufacturerDAO extends GenericDAO<ManufacturerVO, Integer> {
}
