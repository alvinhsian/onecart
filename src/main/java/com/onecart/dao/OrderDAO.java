package com.onecart.dao;

import com.generic.dao.GenericDAO;
import com.onecart.model.OrderVO;

/**
 * Created by Alvin on 2015/4/4.
 */
public interface OrderDAO extends GenericDAO<OrderVO, String> {
}
