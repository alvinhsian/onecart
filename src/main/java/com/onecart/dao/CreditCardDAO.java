package com.onecart.dao;

import com.generic.dao.GenericDAO;
import com.onecart.model.CreditCardVO;

/**
 * Created by Alvin on 2015/4/4.
 */
public interface CreditCardDAO extends GenericDAO<CreditCardVO, Integer> {
}
