package com.onecart.dao.impl;

import com.generic.dao.Impl.GenericDAOImpl;
import com.onecart.dao.OrderDAO;
import com.onecart.model.OrderVO;
import org.springframework.stereotype.Repository;

/**
 * Created by Alvin on 2015/4/4.
 */
@Repository("OrderDAO")
public class OrderDAOImpl extends GenericDAOImpl<OrderVO, String> implements OrderDAO{
}
