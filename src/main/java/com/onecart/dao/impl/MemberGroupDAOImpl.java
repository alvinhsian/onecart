package com.onecart.dao.impl;

import com.generic.dao.Impl.GenericDAOImpl;
import com.onecart.dao.MemberGroupDAO;
import com.onecart.model.MemberGroupVO;
import org.springframework.stereotype.Repository;

/**
 * Created by Alvin on 2015/4/4.
 */
@Repository("MemberGroupDAO")
public class MemberGroupDAOImpl extends GenericDAOImpl<MemberGroupVO, Integer> implements MemberGroupDAO{
}
