package com.onecart.dao.impl;

import com.generic.dao.Impl.GenericDAOImpl;
import com.onecart.dao.ManufacturerDAO;
import com.onecart.model.ManufacturerVO;
import org.springframework.stereotype.Repository;

/**
 * Created by Alvin on 2015/4/4.
 */
@Repository("ManufacturerDAO")
public class ManufacturerDAOImpl extends GenericDAOImpl<ManufacturerVO, Integer> implements ManufacturerDAO {
}
