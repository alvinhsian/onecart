package com.onecart.dao.impl;

import com.generic.dao.Impl.GenericDAOImpl;
import com.onecart.dao.MemberDAO;
import com.onecart.model.MemberVO;
import org.springframework.stereotype.Repository;

/**
 * Created by Alvin on 2015/4/4.
 */
@Repository("MemberDAO")
public class MemberDAOImpl extends GenericDAOImpl<MemberVO, String> implements MemberDAO{
}
