package com.onecart.dao.impl;

import com.generic.dao.Impl.GenericDAOImpl;
import com.onecart.dao.PaymentDAO;
import com.onecart.model.PaymentVO;
import org.springframework.stereotype.Repository;

/**
 * Created by Alvin on 2015/4/4.
 */
@Repository("PaymentDAO")
public class PaymentDAOImpl extends GenericDAOImpl<PaymentVO, Integer> implements PaymentDAO {
}
