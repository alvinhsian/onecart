package com.onecart.dao.impl;

import com.generic.dao.Impl.GenericDAOImpl;
import com.onecart.dao.CreditCardDAO;
import com.onecart.model.CreditCardVO;
import org.springframework.stereotype.Repository;

/**
 * Created by Alvin on 2015/4/4.
 */
@Repository("CreditCardDAO")
public class CreditCardDAOImpl extends GenericDAOImpl<CreditCardVO, Integer> implements CreditCardDAO {
}
