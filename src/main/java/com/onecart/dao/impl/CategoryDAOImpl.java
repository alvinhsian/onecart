package com.onecart.dao.impl;

import com.generic.dao.Impl.GenericDAOImpl;
import com.onecart.dao.CategoryDAO;
import com.onecart.model.CategoryVO;
import org.springframework.stereotype.Repository;

/**
 * Created by Alvin on 2015/4/4.
 */
@Repository("CategoryDAO")
public class CategoryDAOImpl extends GenericDAOImpl<CategoryVO, Integer> implements CategoryDAO {
}
