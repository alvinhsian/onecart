package com.onecart.dao.impl;

import com.generic.dao.Impl.GenericDAOImpl;
import com.onecart.dao.OrderItemDAO;
import com.onecart.model.OrderItemVO;
import org.springframework.stereotype.Repository;

/**
 * Created by Alvin on 2015/4/4.
 */
@Repository("OrderItemDAO")
public class OrderItemDAOImpl extends GenericDAOImpl<OrderItemVO, String> implements OrderItemDAO{
}
