package com.onecart.dao.impl;

import com.generic.dao.Impl.GenericDAOImpl;
import com.onecart.dao.ProductDAO;
import com.onecart.model.ProductVO;
import org.springframework.stereotype.Repository;

/**
 * Created by Alvin on 2015/4/5.
 */
@Repository("ProductDAO")
public class ProductDAOImpl extends GenericDAOImpl<ProductVO, Integer> implements ProductDAO {
}
