package com.onecart.dao;

import com.generic.dao.GenericDAO;
import com.onecart.model.ProductVO;

/**
 * Created by Alvin on 2015/4/5.
 */
public interface ProductDAO extends GenericDAO<ProductVO, Integer> {
}
