package com.onecart.exception;

/**
 * Created by Alvin on 2015/6/21.
 */
public class MemberExistException extends Exception{
    public MemberExistException(String errorMsg){
        super(errorMsg);
    }
}
