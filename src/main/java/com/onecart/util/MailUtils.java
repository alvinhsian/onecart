package com.onecart.util;

import org.apache.commons.mail.*;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Alvin on 2015/6/15.
 */
public class MailUtils {
    private String hostName;
    private String account;
    private String password;
    private String setTo;
    private String setFrom;
    private String fromName;
    private String subject;
    private String message;
    private String attachmentPath;
    private String attachmentURL;
    private String attachmentDescription;
    private String attachmentName;

    public MailUtils(String hostName, String account, String password, String setTo, String setFrom, String fromName, String subject, String message) {
        this.hostName = hostName;
        this.account = account;
        this.password = password;
        this.setTo = setTo;
        this.setFrom = setFrom;
        this.fromName = fromName;
        this.subject = subject;
        this.message = message;
    }

    public MailUtils(String hostName, String account, String password, String setTo, String setFrom, String fromName, String subject, String message, String attachmentPath, String attachmentURL, String attachmentDescription, String attachmentName) {
        this.hostName = hostName;
        this.account = account;
        this.password = password;
        this.setTo = setTo;
        this.setFrom = setFrom;
        this.fromName = fromName;
        this.subject = subject;
        this.message = message;
        this.attachmentPath = attachmentPath;
        this.attachmentURL = attachmentURL;
        this.attachmentDescription = attachmentDescription;
        this.attachmentName = attachmentName;
    }


    public void simpleMail() throws EmailException {
        SimpleEmail email = new SimpleEmail();
        email.setHostName(hostName);
        email.setAuthentication(account, password); // 郵件服務器驗證：用戶名/密碼
        email.setCharset("UTF-8"); //  必須放在前面，否則亂碼
        email.addTo(setTo);
        email.setFrom(setFrom, fromName);
        email.setSubject(subject);
        email.setMsg(message);
        email.send();
    }

    public void multiPartEmail() throws EmailException, MalformedURLException {
        MultiPartEmail email = new MultiPartEmail();
        email.setHostName(hostName);
        email.setAuthentication(account, password);
        email.setCharset("UTF-8");
        email.addTo(setTo);
        email.setFrom(setFrom, fromName);
        email.setSubject(subject);
        email.setMsg(message);

        EmailAttachment emailAttachment = new EmailAttachment();
        emailAttachment.setPath("".equals(attachmentPath) || attachmentPath == null ? "" : attachmentPath); //  本地文件
        emailAttachment.setURL(new URL("".equals(attachmentURL) || attachmentURL == null ? "" : attachmentURL)); // 遠程文件
        emailAttachment.setDisposition(EmailAttachment.ATTACHMENT);
        emailAttachment.setDescription(attachmentDescription);
        emailAttachment.setName(attachmentName);
        email.attach(emailAttachment);
        email.send();
    }

    public void htmlEmail() throws EmailException {
        HtmlEmail email = new HtmlEmail();
        email.setHostName(hostName);
        email.setAuthentication(account, password);
        email.setCharset("UTF-8");
        email.addTo(setTo);
        email.setFrom(setFrom, fromName);
        email.setSubject(subject);
        email.setHtmlMsg("<b>" + message + "</b>");
        email.send();
    }
}
