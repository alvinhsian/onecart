package com.onecart.service.Impl;
import com.generic.dao.GenericDAO;
import com.generic.service.Impl.GenericServiceImpl;
import com.onecart.dao.MemberDAO;
import com.onecart.model.MemberVO;
import com.onecart.service.MemberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


/**
 * Created by Alvin on 2015/6/20.
 */
@Service
public class MemberServiceImpl extends GenericServiceImpl<MemberVO, String> implements MemberService{
    @Qualifier("MemberDAO")
    @Autowired
    private MemberDAO memberDAO;
    public MemberServiceImpl(){
    }

    @Autowired
    public MemberServiceImpl( GenericDAO<MemberVO, String> genericDAO){
        super(genericDAO);
        this.memberDAO = (MemberDAO) genericDAO;
    }


}
