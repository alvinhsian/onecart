package com.onecart.service;

import com.generic.service.GenericService;
import com.onecart.model.MemberVO;

/**
 * Created by Alvin on 2015/6/20.
 */
public interface MemberService extends GenericService<MemberVO, String> {

}
