package com.generic.dao;

import org.hibernate.Query;

import java.util.Collection;
import java.util.List;

/**
 * STEP:1
 * Created by Alvin on 2015/4/4.
 */
public interface GenericDAO <E, K>{

    public void create(E entity);

    public Integer createAll(Collection<E> entities);

    public void update(E entity);

    public Integer updateAll(Collection<E> entities);

    public void delete(E entity);

    public void deleteAll(Collection<E> entities);

    public void saveOrUpdate(E entity);

    public E findById(K id);

    public List<E> getAll();

    public Query createQuery(String hql, Object... params);

    public Query createPageQuery(String hql, int offset, int pageSize, Object... params);

}

