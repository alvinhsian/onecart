package com.generic.dao.Impl;

import com.generic.dao.GenericDAO;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
/**
 * STEP:2
 * Created by Alvin on 2015/4/4.
 * http://www.codeproject.com/Articles/251166/The-Generic-DAO-pattern-in-Java-with-Spring-and
 * http://www.codejava.net/frameworks/spring/spring-4-and-hibernate-4-integration-tutorial-part-1-xml-configuration
 * http://stackoverflow.com/questions/25260527/obtaining-entitymanager-in-spring-hibernate-configuration
 * http://blog.csdn.net/geekjoker/article/details/9421863
 */
@Component
public abstract class GenericDAOImpl<E, K extends Serializable> extends HibernateDaoSupport implements GenericDAO<E, K> {

//    @Autowired(required = true)
//    @Qualifier("sessionFactory")
//    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactoryOverride(SessionFactory sessionFactory){
        /*
        * 當GenericDAO實作類別繼承了HibernateDaoSupport，SessionFactory必須被注入
        * 否則將拋出java.lang.IllegalArgumentException: 'sessionFactory' or 'hibernateTemplate' is required這樣的異常
        * */
        super.setSessionFactory(sessionFactory);
    }

//    @PersistenceContext
//    protected EntityManager entityManager;
    protected Class<? extends E> type;

    public GenericDAOImpl(){
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }
    public Session getSession(){
//        Session session = entityManager.unwrap(Session.class);
        Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
        return session;
    }

    @Override
    public void create(final E entity) {
//        this.entityManager.persist(entity);
        getHibernateTemplate().save(entity);
    }

    @Override
    public Integer createAll(Collection<E> entities){
        int i = 0;
        for(E entity:entities){
            getHibernateTemplate().save(entity);
            i++;
        }
        return i;
    }

    @Override
    public void update(final E entity) {
//        this.entityManager.merge(entity);
        getHibernateTemplate().update(entity);
    }

    @Override
    public Integer updateAll(Collection<E> entities){
        int i = 0;
        for(E entity:entities){
            getHibernateTemplate().update(entity);
            i++;
        }
        return i;
    }

    @Override
    public void delete(E entity) {
        getHibernateTemplate().delete(entity);
    }

    @Override
    public void deleteAll(Collection<E> entities) {
        getHibernateTemplate().deleteAll(entities);
    }

    @Override
    public void saveOrUpdate(E entity) {
        getHibernateTemplate().merge(entity);
    }

    @Override
    public E findById(K id) {
//        return (E)this.entityManager.find(type, id);
        return (E) getSession().get(type, id);
    }

    @Override
    public List<E> getAll() {
        List<E> listElements = (List<E>) getSession().createCriteria(type).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        return listElements;
    }

    @Override
    public org.hibernate.Query createQuery(String hql, Object... params) {
        org.hibernate.Query query = getSession().createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return query;
    }

    @Override
    public org.hibernate.Query createPageQuery(String hql, int offset, int pageSize, Object... params) {
        org.hibernate.Query query = getSession().createQuery(hql);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        query.setFirstResult(offset).setMaxResults(pageSize);
        return query;
    }
}
