package com.generic.constant;

/**
 * Created by Alvin on 2015/6/21.
 * 此為全系統通用的常數
 */
public class CommonConstants {
    /**
     * Member放入Session中的Key值
     * */
    public static final String MEMBER_CONTEXT = "MEMBER_CONTEXT";

    /**
     * 將登入前的URL放入Session中的Key值
     * */
    public static final String LOGIN_TO_URL = "TO_URL";

    /**
     * 每頁幾筆記錄
     * */
    public static final int PAGE_SIZE = 20;
}
