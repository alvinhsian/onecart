package com.generic.service;

import org.hibernate.Query;

import java.util.Collection;
import java.util.List;

/**
 * Created by Alvin on 2015/6/15.
 */
public interface GenericService<E, K> {
    public void saveOrUpdate(E entity);
    public List<E> getAll();
    public E findById(K id);
    public void create(E entity);
    public Integer createAll(Collection<E> entities);
    public void update(E entity);
    public Integer updateAll(Collection<E> entities);
    public void delete(E entity);
    public void deleteAll(Collection<E> entities);
    public Query createQuery(String hql, Object... params);
    public Query createPageQuery(String hql, int offset, int pageSize, Object... params);
}
