package com.generic.service.Impl;

import com.generic.dao.GenericDAO;
import com.generic.service.GenericService;
import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Created by Alvin on 2015/6/15.
 * http://www.codesenior.com/en/tutorial/Spring-Generic-DAO-and-Generic-Service-Implementation
 * http://www.codejava.net/frameworks/spring/spring-4-and-hibernate-4-integration-tutorial-part-2-java-based-configuration
 */
public abstract class GenericServiceImpl <E, K> implements GenericService<E, K> {

    private GenericDAO<E, K> genericDAO;
    public GenericServiceImpl(GenericDAO<E, K> genericDAO){
        this.genericDAO = genericDAO;
    }

    public GenericServiceImpl(){

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdate(E entity) {
        genericDAO.saveOrUpdate(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<E> getAll() {
        return genericDAO.getAll();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public E findById(K id) {
        return genericDAO.findById(id);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void create(E entity) {
        genericDAO.create(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Integer createAll(Collection<E> entities) {
        return genericDAO.createAll(entities);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(E entity) {
        genericDAO.update(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Integer updateAll(Collection<E> entities) {
        return genericDAO.updateAll(entities);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(E entity) {
        genericDAO.delete(entity);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteAll(Collection<E> entities) {
        genericDAO.deleteAll(entities);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Query createQuery(String hql, Object... params) {
        return genericDAO.createQuery(hql, params);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Query createPageQuery(String hql, int offset, int pageSize, Object... params) {
        return genericDAO.createPageQuery(hql, offset, pageSize, params);
    }
}
